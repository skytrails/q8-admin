import request from '@/utils/request'

const orderApi = {
  List: '/manager/order/subOrder',
  Statistics: '/manager/order/subOrder/stats'
}

export const setting = (data) => request.put(`/manager/setting/setting/put/${data.key}`, data)
