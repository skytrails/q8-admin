import request from '@/utils/request'

const api = {
  memberGrade: '/manager/member/memberGrade/all', // 获取会员等级列表
}

export default api

export function getMemberGrade(parameter) {
  return request({
    url: api.memberGrade,
    method: 'get',
    params: parameter,
  })
}
