import request from '@/utils/request'

const api = {
  goodsList: '/manager/goods/goods/sku/list', // 分页获取商品SKU列表
  goodsShelf: '/manager/goods/goods/shelf', // 批量上架下架商品
  goodsCreate: '/manager/goods/goods/create', // 新增商品
  goodsUpdate: '/manager/goods/goods/update/', // 修改商品
  goodsDetails: '/manager/goods/goods/get/', // 商品详情
}

export default api

export function getGoodsList(parameter) {
  return request({
    url: api.goodsList,
    method: 'get',
    params: parameter,
  })
}

export function goodsShelf(parameter) {
  return request({
    url: api.goodsShelf,
    method: 'put',
    data: parameter,
  })
}

export function goodsCreate(parameter) {
  return request({
    url: api.goodsCreate,
    method: 'post',
    data: parameter,
  })
}

export function goodsUpdate(parameter) {
  return request({
    url: api.goodsUpdate + parameter.id,
    method: 'put',
    data: parameter,
  })
}

export function goodsDetails(parameter) {
  return request({
    url: api.goodsDetails + parameter.id,
    method: 'get',
    data: parameter,
  })
}
