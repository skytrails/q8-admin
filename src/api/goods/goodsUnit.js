import request from '@/utils/request'

const api = {
  allUnit: '/manager/goods/goodsUnit', // 获取所有可用品牌
}

export default api


export function getAllBrandList(parameter) {
    return request({
      url: api.allBrand,
      method: 'get',
      params: parameter,
    })
  }

