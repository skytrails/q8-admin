import request from '@/utils/request'

const api = {
  categoryList: '/manager/goods/category', // 查询分类列表
  addOrEditCategoryList: '/manager/goods/category', // 添加或编辑商品分类
  allCategoryList: '/manager/goods/category/allChildren', // 查询全部分类列表
  categoryNavigate: '/manager/goods/category/navigate', // 是否显示到导航栏 分类
  categoryShow: '/manager/goods/category/show', // 显示-隐藏 分类
  categoryDelete: '/manager/goods/category/', // 通过id删除分类
}

export default api

export function categoryDelete(parameter) {
  return request({
    url: api.categoryDelete + '/' + parameter.id,
    method: 'delete',
    data: parameter,
  })
}

export function getCategoryList(parameter) {
  return request({
    url: api.categoryList,
    method: 'get',
    params: parameter,
  })
}

export function getAllCategoryList(parameter) {
  return request({
    url: api.allCategoryList,
    method: 'get',
    params: parameter,
  })
}

// id == 0 add     post
// id != 0 update  put
export function addOrEditCategoryList(parameter) {
  return request({
    url: api.addOrEditCategoryList,
    method: parameter.id ? 'put' : 'post',
    data: parameter,
  })
}

export function categoryNavigate(parameter) {
  return request({
    url: api.categoryNavigate + '/' + parameter.id,
    method: 'put',
    data: parameter,
  })
}

export function categoryShow(parameter) {
  return request({
    url: api.categoryShow,
    method: 'put',
    data: parameter,
  })
}
