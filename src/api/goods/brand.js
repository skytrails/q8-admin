import request from '@/utils/request'

const api = {
  allBrand: '/manager/goods/brand/all', // 获取所有可用品牌
  brand: '/manager/goods/brand/getByPage', // 分页获取可用品牌
}

export default api

export function getBrandList(parameter) {
  return request({
    url: api.brand,
    method: 'get',
    params: parameter,
  })
}

export function getAllBrandList(parameter) {
    return request({
      url: api.allBrand,
      method: 'get',
      params: parameter,
    })
  }

// id == 0 add     post
// id != 0 update  put
export function addOrEditCategoryList(parameter) {
  return request({
    url: api.addOrEditCategoryList,
    method: parameter.id === 0 ? 'post' : 'put',
    data: parameter,
  })
}
