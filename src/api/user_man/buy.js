/**
 * 购买力接口
 */
import request from '@/utils/request'

const buyApi = {
  List: '/manager/statistics/index'
}

/**
 * 查询列表
 * @param parameter
 * @returns {*}
 */
export function getList(parameter) {
  return request({
    url: buyApi.List,
    method: 'get',
    params: parameter
  })
}