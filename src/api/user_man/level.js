/**
 * 会员等级管理接口
 */
import request from '@/utils/request'

const levelApi = {
  List: '/manager/member/memberGrade/getByPage',
  Add: '/manager/member/memberGrade/add',
  Edit: '/manager/member/memberGrade/update'
}

/**
 * 查询列表
 * @param parameter
 * @returns {*}
 */
export function getList(parameter) {
  return request({
    url: levelApi.List,
    method: 'get',
    params: parameter
  })
}


/**
 * 添加
 * @param parameter
 * @returns {*}
 */
export function levelAdd(parameter) {
  return request({
    url: levelApi.Add,
    method: 'post',
    params: parameter
  })
}

/**
 * 编辑
 * @param parameter
 * @returns {*}
 */
export function levelEdit(parameter) {
  return request({
    url: levelApi.Edit + '/' + parameter.id,
    method: 'put',
    params: parameter
  })
}

/**
 * 删除
 * @param parameter
 * @returns {*}
 */
export function levelDel(parameter) {
  return request({
    url: levelApi.List + '/' + parameter,
    method: 'delete'
  })
}