/**
 * 用户管理接口
 */
import request from '@/utils/request'

const userApi = {
  member: '/manager/passport/member',
  importMember: '/manager/passport/member/importMember',
  updateMemberStatus: '/manager/passport/member/updateMemberStatus',
  memberNum: '/manager/passport/member/num',
  delByIds: '/manager/passport/member/delByIds',
  memberAddress: '/manager/member/address',
}

/**
 * 查询列表
 * @param parameter
 * @returns {*}
 */
export function getList(parameter) {
  return request({
    url: userApi.member,
    method: 'get',
    params: parameter
  })
}

/**
 * 添加
 * @param parameter
 * @returns {*}
 */
export function addMember(parameter) {
  return request({
    url: userApi.member,
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑
 * @param parameter
 * @returns {*}
 */
export function editMember(parameter) {
  return request({
    url: userApi.member,
    method: 'put',
    data: parameter
  })
}

/**
 * 查询用户信息
 * @param parameter
 * @returns {*}
 */
export function getUserDetail(parameter) {
  return request({
    url: userApi.member + '/' + parameter,
    method: 'get'
  })
}

/**
 * 根据条件查询会员总数
 * @param parameter
 * @returns {*}
 */
export function getUserNum(parameter) {
  return request({
    url: userApi.memberNum,
    method: 'get',
    params: parameter
  })
}

/**
 * 更改会员状态
 * @param parameter
 * @returns {*}
 */
export function updateMemberStatus(parameter) {
  return request({
    url: userApi.updateMemberStatus,
    method: 'put',
    data: parameter
  })
}

/**
 * 删除
 * @param parameter
 * @returns {*}
 */
export function delMenber(parameter) {
  return request({
    url: userApi.delByIds,
    method: 'post',
    data: parameter
  })
}

/**
 * 导入会员
 * @param parameter
 * @returns {*}
 */
export function importMember(parameter) {
  return request({
    url: userApi.importMember,
    method: 'post',
    params: parameter
  })
}

/**
 * 会员地址
 * @param parameter
 * @returns {*}
 */
export function memberAddress(parameter) {
  return request({
    url: userApi.memberAddress + '/' + parameter.memberId,
    method: 'get',
    params: parameter
  })
}