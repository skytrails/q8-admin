/**
 * 标签管理接口
 */
import request from '@/utils/request'

const labelApi = {
  List: '/manager/label/labelMember',
  Add: '/manager/label/labelMember/add',
  Edit: '/manager/label/labelMember/edit'
}

/**
 * 查询列表
 * @param parameter
 * @returns {*}
 */
export function getList(parameter) {
  return request({
    url: labelApi.List,
    method: 'get',
    params: parameter
  })
}

/**
 * 添加
 * @param parameter
 * @returns {*}
 */
export function labelAdd(parameter) {
  return request({
    url: labelApi.Add,
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑
 * @param parameter
 * @returns {*}
 */
export function labelEdit(parameter) {
  return request({
    url: labelApi.Edit + '/' + parameter.id,
    method: 'put',
    params: parameter
  })
}

/**
 * 删除
 * @param parameter
 * @returns {*}
 */
export function labelDel(parameter) {
  return request({
    url: labelApi.List + '/' + parameter,
    method: 'delete'
  })
}