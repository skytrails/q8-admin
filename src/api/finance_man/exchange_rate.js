/**
 * 汇率管理接口
 */
import request from '@/utils/request'

const exchangeApi = {
  List: '/manager/statistics/index'
}

/**
 * 查询列表
 * @param parameter
 * @returns {*}
 */
export function getList(parameter) {
  return request({
    url: exchangeApi.List,
    method: 'get',
    params: parameter
  })
}