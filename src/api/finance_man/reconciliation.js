/**
 * 对账管理接口
 */
import request from '@/utils/request'

const reconciliationApi = {
  List: '/manager/statistics/index'
}

/**
 * 查询列表
 * @param parameter
 * @returns {*}
 */
export function getList(parameter) {
  return request({
    url: reconciliationApi.List,
    method: 'get',
    params: parameter
  })
}