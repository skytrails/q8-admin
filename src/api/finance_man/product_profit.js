/**
 * 产品利润接口
 */
import request from '@/utils/request'

const productApi = {
  List: '/manager/statistics/index'
}

/**
 * 查询列表
 * @param parameter
 * @returns {*}
 */
export function getList(parameter) {
  return request({
    url: productApi.List,
    method: 'get',
    params: parameter
  })
}