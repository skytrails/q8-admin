import request from '@/utils/request'
import qs from 'qs';

// 库存专区
export const list = (params) => request.get('/manager/promotion/stockGoods/getByPage', { params })

export const del = (ids) => request.delete(`/manager/promotion/stockGoods/delByIds/${ids}`)

export const set = (data) => request.post('/manager/promotion/stockGoods/discount', data)

export const add = (data) => request.post('/manager/promotion/stockGoods/create', data)

// 优惠券
export const couponList = (params) => request.get('/manager/promotion/coupon', { params })

export const couponDetail = (params) => request.get(`/manager/promotion/coupon/coupon/${params.id}`, { params })

// 广告
export const adList = (params) => request.get('/manager/other/ad/list', { params })

export const adDetail = (id) => request.get(`/manager/other/ad/detail/${id}`)

export const adDel = (ids) => request.delete(`/manager/other/ad/delByIds/${ids}`)

export const adSetTop = (id) => request.put(`/manager/other/ad/top/${id}`)

export const adAdd = (data) => request.post('/manager/other/ad/add', qs.stringify(data), { headers: { 'content-type': 'application/x-www-form-urlencoded' } })

export const adEdit = (data) => request.put(`/manager/other/ad/edit/${data.id}`, qs.stringify(data), { headers: { 'content-type': 'application/x-www-form-urlencoded' } })

// 活动
export const activityList = (params) => request.get('/manager/platform/activity/list', { params })

export const activityEdit = (data) => request.put(`/manager/platform/activity/edit/${data.id}`, data)

export const activityDetail = (id) => request.get(`/manager/platform/activity/get/${id}`)

// 热词
export const hotWordList = (params) => request.get('/manager/hotwords/hotwords/statistics', { params })