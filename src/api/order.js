import request from '@/utils/request'

const orderApi = {
  List: '/manager/order/subOrder',
  Statistics: '/manager/order/subOrder/stats'
}

export const list = (params) => request.get(orderApi.List, { params })

export const cancel = (data) => request.put(`/manager/order/order/cancel/${data.orderSn}`, data)

export const detail = (sn) => request.get(`/manager/order/subOrder/${sn}`)

export const statistics = () => request.get(orderApi.Statistics)
