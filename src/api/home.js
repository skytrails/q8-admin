/**
 * 首页数据统计接口
 */
import request from '@/utils/request'

const homeApi = {
  HomeIndex: '/manager/statistics/index',
  HotGoods: '/manager/statistics/index/goodsStatistics',
  Notice: '/manager/statistics/index/notice',
  HotStores: '/manager/statistics/index/storeStatistics',
  Logs: '/manager/setting/log/getAllByPage',
  DeleteAllLogs: '/manager/setting/log',
  DeleteLogsIds: '/manager/setting/log'
}

/**
 * 获取首页查询数据
 * @param parameter
 * @returns {*}
 */
export function homeList(parameter) {
  return request({
    url: homeApi.HomeIndex,
    method: 'get',
    params: parameter
  })
}

/**
 * 获取首页查询热卖商品TOP10
 * @param {*} parameter 
 * @returns 
 */
export function hotGoods(parameter) {
  return request({
    url: homeApi.HotGoods,
    method: 'get',
    params: parameter
  })
}

/**
 * 通知提示信息
 * @param {*} parameter 
 * @returns 
 */
export function notice(parameter) {
  return request({
    url: homeApi.Notice,
    method: 'get',
    params: parameter
  })
}

/**
 * 获取首页查询热卖店铺TOP10
 * @param {*} parameter 
 * @returns 
 */
export function hotStores(parameter) {
  return request({
    url: homeApi.HotStores,
    method: 'get',
    params: parameter
  })
}

/**
 * 获取全部日志
 * @param {*} parameter 
 * @returns 
 */
export function logsList(parameter) {
  return request({
    url: homeApi.Logs,
    method: 'get',
    params: parameter
  })
}

/**
 * 删除全部日志
 * @param {*} parameter 
 * @returns 
 */
export function delAllLogs(parameter) {
  return request({
    url: homeApi.DeleteAllLogs,
    method: 'delete',
    params: parameter
  })
}

/**
 * 批量删除日志
 * @param {*} parameter 
 * @returns 
 */
export function delLogsIds(parameter) {
  return request({
    url: homeApi.DeleteLogsIds,
    method: 'delete',
    params: parameter
  })
}