import Mock from 'mockjs2'
import { builder } from '../util'

const info = options => {
  console.log('options', options)
  const userInfo = {
    id: '4291d7da9005377ec9aec4a71ea837f',
    name: '天野远子',
    username: 'admin',
    password: '',
    avatar: '/avatar2.jpg',
    status: 1,
    telephone: '',
    lastLoginIp: '27.154.74.117',
    lastLoginTime: 1534837621348,
    creatorId: 'admin',
    createTime: 1497160610259,
    merchantCode: 'TLif2btpzg079h15bk',
    deleted: 0,
    roleId: 'admin',
    role: {}
  }
  // role
  const roleObj = {
    id: 'admin',
    name: '管理员',
    describe: '拥有所有权限',
    status: 1,
    creatorId: 'system',
    createTime: 1497160610259,
    deleted: 0,
    permissions: [
      {
        roleId: 'admin',
        permissionId: 'dashboard',
        permissionName: '仪表盘',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'exception',
        permissionName: '异常页面权限',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'result',
        permissionName: '结果权限',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'profile',
        permissionName: '详细页权限',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'table',
        permissionName: '表格权限',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'import',
            describe: '导入',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'form',
        permissionName: '表单权限',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'order',
        permissionName: '订单管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'permission',
        permissionName: '权限管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'role',
        permissionName: '角色管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'table',
        permissionName: '桌子管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'user',
        permissionName: '用户管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"},{"action":"export","defaultCheck":false,"describe":"导出"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'import',
            describe: '导入',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          },
          {
            action: 'export',
            describe: '导出',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      }
    ]
  }

  roleObj.permissions.push({
    roleId: 'admin',
    permissionId: 'support',
    permissionName: '超级模块',
    actions:
      '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"},{"action":"export","defaultCheck":false,"describe":"导出"}]',
    actionEntitySet: [
      {
        action: 'add',
        describe: '新增',
        defaultCheck: false
      },
      {
        action: 'import',
        describe: '导入',
        defaultCheck: false
      },
      {
        action: 'get',
        describe: '详情',
        defaultCheck: false
      },
      {
        action: 'update',
        describe: '修改',
        defaultCheck: false
      },
      {
        action: 'delete',
        describe: '删除',
        defaultCheck: false
      },
      {
        action: 'export',
        describe: '导出',
        defaultCheck: false
      }
    ],
    actionList: null,
    dataAccess: null
  })

  userInfo.role = roleObj
  return builder(userInfo)
}

/**
 * 使用 用户登录的 token 获取用户有权限的菜单
 * 返回结构必须按照这个结构体形式处理，或根据
 * /src/router/generator-routers.js  文件的菜单结构处理函数对应即可
 * @param {*} options
 * @returns
 */
const userNav = options => {
  const nav = [
    // dashboard
    {
      name: 'home',
      parentId: 0,
      id: 1,
      meta: {
        icon: 'dashboard',
        title: '首页',
        show: true
      },
      component: 'RouteView',
      redirect: '/home/home'
    },
    {
      name: 'home',
      parentId: 1,
      id: 7,
      meta: {
        title: '系统首页',
        show: true
      },
      component: 'Home'
    },
    {
      name: 'AccountSetting',
      parentId: 1,
      id: 2,
      meta: {
        title: '账户设置',
        show: true
      },
      component: 'AccountSetting',
      path: '/home/accountSetting'
    },
    {
      name: 'SystemInfo',
      parentId: 1,
      id: 2,
      meta: {
        title: '系统信息',
        show: true
      },
      component: 'SystemInfo',
      path: '/home/systemInfo'
    },
    {
      name: 'LoginLog',
      parentId: 1,
      id: 2,
      meta: {
        title: '登录日志',
        show: true
      },
      component: 'LoginLog',
      path: '/home/loginLog'
    },

    {
      name: 'goods',
      parentId: 0,
      id: 101,
      meta: {
        icon: 'dashboard',
        title: '商品管理',
        show: true
      },
      component: 'RouteView',
      redirect: '/goods/goodsList'
    },
    {
      name: 'goodsList',
      parentId: 101,
      id: 1010,
      meta: {
        title: '商品列表',
        show: true
      },
      component: 'GoodsList'
    },
    {
      name: 'addGoods',
      parentId: 101,
      id: 1011,
      meta: {
        title: '添加商品',
        show: true
      },
      component: 'AddGoods'
    },
    {
      name: 'goodsRecycleBin',
      parentId: 101,
      id: 1013,
      meta: {
        title: '商品回收站',
        show: true
      },
      component: 'GoodsRecycleBin'
    },
    {
      name: 'goodsBatch',
      parentId: 101,
      id: 1014,
      meta: {
        title: '商品回收站',
        show: false
      },
      component: 'GoodsBatch'
    },
    {
      name: 'category',
      parentId: 101,
      id: 1015,
      meta: {
        title: '商品分类',
        show: true
      },
      component: 'Category'
    },
    {
      name: 'addCategory',
      parentId: 101,
      id: 1016,
      meta: {
        title: '添加商品分类',
        show: false,
      },
      component: 'AddCategory'
    },


    {
      name: 'brand',
      parentId: 101,
      id: 1017,
      meta: {
        title: '品牌管理',
        show: true,
      },
      component: 'Brand'
    },

    {
      name: 'actingBrand',
      parentId: 101,
      id: 1018,
      meta: {
        title: '代理品牌管理',
        show: true,
      },
      component: 'Brand'
    },
    {
      name: 'addBrand',
      parentId: 101,
      id: 1019,
      meta: {
        title: '添加品牌',
        show: false,
      },
      component: 'AddBrand'
    },
    {
      name: 'batchShipping',
      parentId: 101,
      id: 1020,
      meta: {
        title: '商品批量发货',
        show: true,
      },
      component: 'BatchShipping'
    },
    {
      name: 'albumLibrary',
      parentId: 101,
      id: 1021,
      meta: {
        title: '图片库管理',
        show: true,
      },
      component: 'AlbumLibrary'
    },
    {
      name: 'addAlbum',
      parentId: 101,
      id: 1022,
      meta: {
        title: '新建相册',
        show: false,
      },
      component: 'AddAlbum'
    },
    {
      name: 'imageList',
      parentId: 101,
      id: 1023,
      meta: {
        title: 'ImageList',
        show: false,
      },
      component: 'ImageList'
    },


    // 订单管理
    {
      name: 'order',
      parentId: 0,
      id: 103,
      meta: {
        icon: 'dashboard',
        title: '订单管理',
        show: true
      },
      component: 'RouteView',
      redirect: '/order/orderList'
    },
    {
      name: 'orderList',
      parentId: 103,
      id: 1030,
      meta: {
        title: '订单列表',
        show: true
      },
      component: 'OrderList'
    },
    {
      name: 'dataStatistics',
      parentId: 0,
      id: 20220808,
      meta: {
        icon: 'dashboard',
        title: '数据统计',
        show: true
      },
      component: 'RouteView',
      redirect: '/dataStatistics/productListChart'
    },
    {
      name: 'productListChart',
      parentId: 20220808,
      id: 80801,
      meta: {
        title: '平台销售数据',
        show: true
      },
      component: 'ProductListChart'
    },
    {
      name: 'shopListChart',
      parentId: 20220808,
      id: 80802,
      meta: {
        title: '商品销售数据',
        show: true
      },
      component: 'ShopListChart'
    },
    {
      name: 'goodsListChart',
      parentId: 20220808,
      id: 80803,
      meta: {
        title: '订单用户数据',
        show: true
      },
      component: 'GoodsListChart'
    },
    {
      name: 'profitListChart',
      parentId: 20220808,
      id: 80804,
      meta: {
        title: '商品利润数据',
        show: true
      },
      component: 'ProfitListChart'
    },

    {
      name: 'storeManage',
      parentId: 0,
      id: 202208072,
      meta: {
        icon: 'dashboard',
        title: '仓储管理',
        show: true
      },
      component: 'RouteView',
      redirect: '/storeManage/warehousing'
    },
    {
      name: 'warehousing',
      parentId: 202208072,
      id: 80702,
      meta: {
        title: '入库管理',
        show: true
      },
      component: 'Warehousing'
    },
    {
      name: 'wareDetail',
      parentId: 202208072,
      id: 8070201,
      meta: {
        title: '',
        show: false,
      },
      component: 'WareDetail'
    },
    {
      name: 'stockManage',
      parentId: 202208072,
      id: 8070202,
      meta: {
        title: '库存管理',
        show: true
      },
      component: 'StockManage'
    },
    {
      name: 'stockDetail',
      parentId: 202208072,
      id: 8070204,
      meta: {
        title: '',
        show: false
      },
      component: 'StockDetail'
    },
    {
      name: 'exwarehouse',
      parentId: 202208072,
      id: 80703,
      meta: {
        title: '出库管理',
        show: true
      },
      component: 'Exwarehouse'
    },
    {
      name: 'exwarehouseDetail',
      parentId: 202208072,
      id: 8070301,
      meta: {
        title: '',
        show: false,
      },
      component: 'ExwareDetail'
    },
    {
      name: 'pickUpAppoint',
      parentId: 0,
      id: 220809,
      meta: {
        title: '取货预约管理',
        icon: 'dashboard',
        show: true
      },
      component: 'RouteView',
      redirect: '/pickUpAppoint/takeAppointGood'
    },
    {
      name: 'takeAppointGood',
      parentId: 220809,
      id: 8091,
      meta: {
        title: '预约提货管理',
        show: true
      },
      component: 'TakeAppointGood'
    },
    { 
      name: 'deliveryAppointGood',
      parentId: 220809,
      id: 8092,
      meta: {
        title: '预约送货管理',
        show: true
      },
      component: 'DeliveryAppointGood'
    },
    {
      name: 'repairMan',
      parentId: 0,
      id: 19,
      meta: {
        icon: 'dashboard',
        title: '维修管理',
        show: true
      },
      component: 'RepairMan',
      redirect: '/repair/RepairMan'
    },
    {
      name: 'repairMan',
      parentId: 19,
      id: 191,
      meta: {
        title: '维修管理列表',
        show: true
      },
      component: 'RepairMan'
    },

    {
      name: 'systemMan',
      parentId: 0,
      id: 20,
      meta: {
        icon: 'dashboard',
        title: '系统管理',
        show: true
      },
      component: 'RouteView',
      redirect: '/systemMan/department'
    },
    {
      name: 'department',
      parentId: 20,
      id: 22,
      meta: {
        title: '部门管理',
        show: true
      },
      component: 'Department'
    },
    {
      name: 'permission',
      parentId: 20,
      id: 28,
      meta: {
        title: '权限管理',
        show: false
      },
      component: 'Permission'
    },
    {
      name: 'member',
      parentId: 20,
      id: 31,
      meta: {
        title: '成员管理',
        show: true
      },
      component: 'Member'
    },
    {
      name: 'addMember',
      parentId: 20,
      id: 29,
      meta: {
        title: '添加成员',
        show: false
      },
      component: 'AddMember'
    },
    {
      name: 'operationLog',
      parentId: 20,
      id: 30,
      meta: {
        title: '操作日志',
        show: true
      },
      component: 'OperationLog'
    },
  ]
  const json = builder(nav)
  console.log('json', json)
  return json
}

Mock.mock(/\/api\/user\/info/, 'get', info)
Mock.mock(/\/api\/user\/nav/, 'get', userNav)
