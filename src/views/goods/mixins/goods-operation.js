import { goodsShelf } from '@/api/goods/list'
export default {
  data() {
    return {
      shelfVisible: false,
      shelfConfirmLoading: false,
      currentGoodsShelfData: {
        shelfFlag: true,
        skuIds: [],
        isBatch: false,
      },

      deleteVisible: false,
      deleteConfirmLoading: false,
      currentGoodsDeleteData: {
        ids: [],
        isBatch: false,
      },
    }
  },
  computed: {
    shelfModalText() {
      let { shelfFlag, isBatch } = this.currentGoodsShelfData
      let statusText = shelfFlag ? '上架' : '下架'
      let isBatchText = isBatch ? '批量' : ''
      return `确认${isBatchText}${statusText}吗？`
    },
    deleteModalText() {
      let { isBatch } = this.currentGoodsDeleteData
      let statusText = '删除'
      let isBatchText = isBatch ? '批量' : ''
      return `确认${isBatchText}${statusText}吗？`
    },
  },
  methods: {
    setBatchDeleteData() {
      this.currentGoodsShelfData = {
        skuIds: this.selectedRowKeys,
        isBatch: true,
      }
      this.deleteShowModal()
    },
    deleteShowModal() {
      this.deleteVisible = true
    },
    deleteHandleOk(e) {
      this.openGoodsDelete()
    },
    deleteHandleCancel(e) {
      this.deleteVisible = false
      this.deleteConfirmLoading = false
    },
    // 批量删除商品
    openGoodsDelete(data) {
      this.deleteConfirmLoading = true
      return new Promise((resolve) => {
        goodsShelf(this.currentGoodsDeleteData)
          .then((res) => {
            let { code } = res
            if (code === 200) {
              this.$refs.table.refresh()
              this.deleteVisible = false
            }
          })
          .finally(() => {
            this.deleteConfirmLoading = false
            resolve()
          })
      })
    },

    setBatchShelfData(shelfFlag) {
      this.currentGoodsShelfData = {
        shelfFlag,
        skuIds: this.selectedRowKeys,
        isBatch: true,
      }
      this.shelfShowModal()
    },
    shelfShowModal() {
      this.shelfVisible = true
    },
    shelfHandleOk(e) {
      this.openGoodsShelf()
    },
    shelfHandleCancel(e) {
      this.shelfVisible = false
      this.shelfConfirmLoading = false
    },
    // 批量上架下架商品
    openGoodsShelf(data) {
      this.shelfConfirmLoading = true
      return new Promise((resolve) => {
        goodsShelf(this.currentGoodsShelfData)
          .then((res) => {
            let { code } = res
            if (code === 200) {
              this.$refs.table.refresh()
              this.shelfVisible = false
            }
          })
          .finally(() => {
            this.shelfConfirmLoading = false
            resolve()
          })
      })
    },
  },
}
