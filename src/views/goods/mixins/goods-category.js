import { categoryDelete } from '@/api/goods/category'
export default {
  data() {
    return {
      deleteVisible: false,
      deleteConfirmLoading: false,
      currentCategoryDeleteData: {
        ids: [],
        isBatch: false,
      },
    }
  },
  computed: {
    deleteModalText() {
      let { isBatch } = this.currentCategoryDeleteData
      let statusText = '删除'
      let isBatchText = isBatch ? '批量' : ''
      return `确认${isBatchText}${statusText}吗？`
    },
  },
  methods: {
    setBatchDeleteData() {
      this.currentCategoryShelfData = {
        skuIds: this.selectedRowKeys,
        isBatch: true,
      }
      this.deleteShowModal()
    },
    deleteShowModal() {
      this.deleteVisible = true
    },
    deleteHandleOk(e) {
      this.openCategoryDelete()
    },
    deleteHandleCancel(e) {
      this.deleteVisible = false
      this.deleteConfirmLoading = false
    },
    // 批量删除商品
    openCategoryDelete(data) {
      this.deleteConfirmLoading = true
      return new Promise((resolve) => {
        categoryDelete(this.currentCategoryDeleteData)
          .then((res) => {
            let { code } = res
            if (code === 200) {
              this.$refs.table.refresh()
              this.deleteVisible = false
            }
          })
          .finally(() => {
            this.deleteConfirmLoading = false
            resolve()
          })
      })
    },
  },
}
