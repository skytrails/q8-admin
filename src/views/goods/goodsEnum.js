export const GoodsRecommendEnum = {
  NEWS_FLAG: 1, // 新品
  RECOMMEND: 2, // 推荐
}

export const GoodsService = {
  RETURN_GOODS: 1, // 快速退货
  QUICK_REFUND: 2, // 快速退款
  FREE_SHIPPING: 3, // 免费包邮
}

export const FooterOperationEnum = {
  BATCH_DELETE: 1, // 批量删除
  BATCH_UP_SHELF: 2, // 批量上架
  BATCH_DOWN_SHELF: 3, // 批量下架
}
