export default {
  data() {
    return {
      selectedRowKeys: [],
      orderOptions: [
        { value: 'asc', label: '正序' },
        { value: 'desc', label: '倒序' },
      ],
      labelCol: { span: 8 },
      wrapperCol: { span: 14 },
      apis: {},
      condition: {},
      conditionWatch: { order: undefined },
      mainKey: 'id',
      actionType: 'add',
      editRoute: '',
    }
  },
  watch: {
    conditionWatch: {
      handler() {
        this.$refs.table?.refresh(true)
      },
      deep: true
    }
  },
  methods: {
    loadData(parameter) {
      const requestParameters = Object.assign({}, parameter, this.condition, this.conditionWatch)
      return this.apis.list(requestParameters)
      // .then(({ result }) => {
      //   return {
      //     pageNo: result.current,
      //     pageSize: result.size,
      //     totalCount: result.total,
      //     totalPage: result.pages,
      //     data: result.records,
      //   }
      // })
      // 与 components/Table/index.js L163 相对应
    },
    commonActions(actionType, params = {}) {
      this.$router.push({
        path: this.editRoute,
        query: {
          actionType,
          ...params
        }
      })
    },
    goAdd() {
      this.commonActions('add')
    },
    goEdit(row) {
      this.commonActions('edit', {
        id: row.id
      })
    },
    goView(row) {
      this.commonActions('view', {
        id: row.id
      })
    },
    goDel(row) {
      if (!this.apis.del) {
        throw new Error('请传递删除api函数')
      }
      this.$confirm({
        title: '确定删除这条数据 ?',
        cancelText: '取消',
        okText: '确认',
        onOk: () => {
          this.apis.del(row[this.mainKey]).then(() => {
            this.$notification.success({
              message: '删除成功'
            })
            this.$refs.table.refresh()
          })
        },
      });
    },
    goBatchDel(row) {
      if (!this.apis.del) {
        throw new Error('请传递删除api函数')
      }
      this.$confirm({
        title: '确定删除这些数据 ?',
        cancelText: '取消',
        okText: '确认',
        onOk: () => {
          this.apis.del(this.selectedRowKeys.join(',')).then(() => {
            this.$notification.success({
              message: '删除成功'
            })
            this.$refs.table.refresh()
          })
        },
      });
    },
    submit() {
      if (!this.$refs.form) {
        throw new Error('请传递ref实例')
      }
      this.$refs.form.validate(valid => {
        if (valid) {
          this.actionType === 'add' ? this._add() : this._edit()
        } else {
          return false;
        }
      });
    },
    getFormData() {
      return this.form
    },
    _add() {
      if (!this.apis.add) {
        throw new Error('请传递添加api函数')
      }
      const data = this.getFormData()
      this.apis.add(data).then(res => {
        this.$notification.success({
          message: '添加成功'
        })
      })
    },
    _edit() {
      if (!this.apis.edit) {
        throw new Error('请传递编辑api函数')
      }
      const data = this.getFormData()
      this.apis.edit(data).then(res => {
        this.$notification.success({
          message: '编辑成功'
        })
      })
    },
    onSelectChange(v) {
      this.selectedRowKeys = v
    },
    mockSelect(v) {
      this.selectedRowKeys = v.target.checked ? this.$refs.table.localDataSource.map(v => v[this.mainKey]) : []
    }
  }
}
