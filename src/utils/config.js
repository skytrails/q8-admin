const BASE_URL = process.env
let baseURL = BASE_URL.VUE_APP_BASEURL

if (BASE_URL.VUE_APP_CURRENTMODE === 'dev') {
  baseURL = 'https://appapitest.56yhl.com/api/' // 测试
}

export { baseURL }
