/**
 * 支付状态
 */
export const payStatus = {
  UNPAID: '待付款',
  PAID: '已付款',
  CANCEL: '已取消',
}

/**
 * 子订单状态
 */
export const orderStatus = {
  UNPAID: '待付款',
  PAID: '已付款',
  WAIT_TRANSIT: '待运输',
  TRANSIT: '运输中',
  STORAGE: '已入库',
  APPLYING: '自提',
  BETAKE: '待提货',
  UNPAID_FREIGHT: '待付运费',
  UNDELIVERED: '待发货',
  DELIVERED: '已发货',
  COMPLETED: '已完成',
  CANCELLED: '已取消',
  CLOSED: '已关闭',
}

// 提货方式 配送方式
export const takeType = {
  DEFAULT: '无预约自提',
  PICKUP: '预约自提',
  DELIVERY: '送货上门',
}

/**
 * 订单类型
 */
export const orderType = {
  NORMAL: '普通订单'
}

/**
 * 支付方式
 */
export const payMethod = {
  KNET: 'KNET支付',
  WALLET: '余额支付',
  BANK_TRANSFER: '线下转账',
}

// 优惠券类型
export const couponType = {
  ALL: '全部',
  POINT: '打折',
  PRICE: '减免现金'
}

// 平台
export const platformType = {
  ALL: '全部',
  APP: 'APP',
  PC: 'PC'
}

// 适用商品
export const scopeType = {
  ALL: '全部',
  PORTION_GOODS_CATEGORY: '指定商品分类',
  PORTION_GOOD: '指定商品',
}

export const adPlace = {
  APP_HOME_SLIDE: 'APP首页轮播',
  APP_SPECIAL_AD: 'APP专区广告',
  APP_SPECIAL_BANNER: 'APP专区Banner图',
  APP_USER_CENTER_AD: 'APP个人中心广告',
  PC_HOME_SLIDE: 'PC端首页轮播',
  PC_CATEGORY_AD: 'PC端分类广告',
  OTHER: '其他'
}