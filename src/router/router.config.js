// eslint-disable-next-line
import { UserLayout, BasicLayout } from '@/layouts'
import { bxAnaalyse } from '@/core/icons'

const RouteView = {
  name: 'RouteView',
  render: h => h('router-view')
}

export const asyncRouterMap = [{
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: 'menu.home' },
    redirect: '/dashboard/workplace',
    children: []
  },
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [{
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [{
      path: 'login',
      name: 'login',
      component: () =>
        import ( /* webpackChunkName: "user" */ '@/views/user/Login')
    }]
  },

  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: 'menu.home' },
    redirect: '/home/index',
    children: [{
        path: '/home',
        name: 'home',
        redirect: '/home/index',
        component: RouteView,
        meta: { title: 'menu.home', keepAlive: true, icon: bxAnaalyse },
        children: [{
          path: '/home/index',
          name: 'homeIndex',
          component: () =>
            import ('@/views/home/Home'),
          meta: { title: 'menu.home.index', keepAlive: true }
        }]
      },
      {
        path: '/userMan',
        name: 'userman',
        redirect: '/userMan/user/List',
        component: RouteView,
        meta: { title: 'menu.userman', keepAlive: true, icon: bxAnaalyse },
        children: [{
          path: '/userMan/user/List',
          name: 'userList',
          component: () =>
            import ('@/views/userMan/user/List'),
          meta: { title: 'menu.userMan.user.List', keepAlive: true }
        }, {
          path: '/userMan/user/Detail',
          name: 'userDetail',
          component: () =>
            import ('@/views/userMan/user/Detail'),
          meta: { title: 'menu.userMan.user.Detail', keepAlive: true }
        }, {
          path: '/userMan/user/Edit',
          name: 'userEdit',
          component: () =>
            import ('@/views/userMan/user/Edit'),
          meta: { title: 'menu.userMan.user.Edit', keepAlive: true }
        }, {
          path: '/userMan/label/List',
          name: 'labelList',
          component: () =>
            import ('@/views/userMan/label/List'),
          meta: { title: 'menu.userMan.label.List', keepAlive: true }
        }, {
          path: '/userMan/level/List',
          name: 'levelList',
          component: () =>
            import ('@/views/userMan/level/List'),
          meta: { title: 'menu.userMan.level.List', keepAlive: true }
        }]
      },
      {
        path: '/goods',
        name: 'goods',
        component: RouteView,
        meta: { title: 'menu.goods', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: '/goods/list',
            name: 'goodsList',
            component: () => import('@/views/goods/GoodsList'),
            meta: { title: 'menu.goods.list', keepAlive: true },
          },
          {
            path: '/goods/add',
            name: 'goodsAdd',
            component: () => import('@/views/goods/AddGoods'),
            meta: { title: 'menu.goods.add', keepAlive: true },
          },
          {
            path: '/goods/goodsBatch',
            name: 'goodsBatch',
            component: () => import('@/views/goods/GoodsBatch'),
            meta: { title: 'menu.goods.goodsBatch', keepAlive: true },
          },
          {
            path: '/goods/category',
            name: 'goodsCategory',
            component: () => import('@/views/goods/Category'),
            meta: { title: 'menu.goods.Category', keepAlive: true },
          },
          {
            path: '/goods/categoryAdd',
            name: 'addCategory',
            component: () => import('@/views/goods/AddCategory'),
            meta: { title: 'menu.goods.goodsCategoryAdd', keepAlive: true },
          },
        ],
      },
      {
        path: '/marketing',
        name: 'marketing',
        component: RouteView,
        meta: { title: 'menu.marketing', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: '/marketing/inStock',
            name: 'inStock',
            component: () => import('@/views/marketing/InStock'),
            meta: { title: 'menu.marketing.inStock', keepAlive: true }
          },
          {
            path: '/marketing/coupon',
            name: 'coupon',
            component: () => import('@/views/marketing/Coupon'),
            meta: { title: 'menu.marketing.coupon', keepAlive: true }
          },
          {
            path: '/marketing/couponDetail',
            name: 'couponDetail',
            component: () => import('@/views/marketing/CouponDetail'),
            meta: { title: 'menu.marketing.couponDetail', keepAlive: true }
          },
          {
            path: '/marketing/ad',
            name: 'ad',
            component: () => import('@/views/marketing/Ad'),
            meta: { title: 'menu.marketing.ad', keepAlive: true }
          },
          {
            path: '/marketing/addAd',
            name: 'addAd',
            component: () => import('@/views/marketing/AddAd'),
            meta: { title: 'menu.marketing.addAd', keepAlive: true }
          },
          {
            path: '/marketing/activity',
            name: 'activity',
            component: () => import('@/views/marketing/Activity'),
            meta: { title: 'menu.marketing.activity', keepAlive: true }
          },
          {
            path: '/marketing/activityAdd',
            name: 'activityAdd',
            component: () => import('@/views/marketing/ActivityAdd'),
            meta: { title: 'menu.marketing.activityAdd', keepAlive: true }
          },
          {
            path: '/marketing/hotWord',
            name: 'hotWord',
            component: () => import('@/views/marketing/HotWord'),
            meta: { title: 'menu.marketing.hotWord', keepAlive: true }
          },
        ]
      },
      {
        path: '/order',
        name: 'order',
        redirect: '/order/list',
        component: RouteView,
        meta: { title: 'menu.order', keepAlive: true, icon: bxAnaalyse },
        children: [{
            path: '/order/list',
            name: 'orderList',
            component: () =>
              import ('@/views/order/OrderList'),
            meta: { title: 'menu.order.list', keepAlive: true }
          },
          {
            path: '/order/detail',
            name: 'orderDetail',
            component: () =>
              import ('@/views/order/OrderDetail'),
            meta: { title: 'menu.order.detail', keepAlive: true }
          },
          {
            path: '/order/cancel',
            name: 'orderCancel',
            component: () =>
              import ('@/views/order/CancelOrder'),
            meta: { title: 'menu.order.cancel', keepAlive: true }
          },
          {
            path: '/order/trace',
            name: 'orderTrace',
            component: () =>
              import ('@/views/order/OrderTrace'),
            meta: { title: 'menu.order.trace', keepAlive: true }
          },
          {
            path: '/order/reminder',
            name: 'orderReminder',
            component: () =>
              import ('@/views/order/ArrivalReminder'),
            meta: { title: 'menu.order.reminder', keepAlive: true }
          },
          {
            path: '/order/setting',
            name: 'orderSetting',
            component: () =>
              import ('@/views/order/OrderSetting'),
            meta: { title: 'menu.order.setting', keepAlive: true }
          },
        ]
      },
    ]
  },

  {
    path: '/404',
    component: () =>
      import ( /* webpackChunkName: "fail" */ '@/views/exception/404')
  }
]